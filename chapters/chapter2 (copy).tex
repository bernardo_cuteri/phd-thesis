
\newcommand{\quotes}[1]{``#1''}

The information need of a user often resolves in a simple question where it would
be useful to have brief answers instead of whole documents to look into. IR techniques have proven to be successful at locating relevant documents to the
user query into large collections, but the effort of looking for a specific desired
information into such documents is then left to the user. Question answering
attempts to find direct answers to user questions.
As the intuition says, answering to any kind of question, with no linguistic and
no domain restriction is a very hard task. When no restriction is made on the
domain of the questions we are talking about open domain question answering while, when questions are bound to a specific domain, we are in
closed (or restricted) domain question answering (CDQA).
In open domain QA, most systems are based on a combination of Information
Retrieval and NLP techniques[]. Such techniques are applied to a large corpora
of documents: first attempting to retrieve the best documents to look into for the
answer, then selecting the paragraphs which are more likely to bear the desired
answer and finally processing the extracted paragraphs by means of NLP. Such
approach is also behind many closed domain question answering systems, but in
this context we might benefit of existing structured knowledge.
Some of the very early question answering systems were designed for closed
domains and they were essentially conceived as natual language interfaces to
databases [][].

In this work we present a closed domain question answering system for the cultural heritage domain that comes from the PIUCULTURA project, which is a project
of which the University of Calabria is a research partner.

Cultural heritage can indeed benefit of structured data sources: in this context, data has already started to be saved and shared with common standards. One of the most successful standard is the CIDOC Conceptual Reference Model, that has been identified as the ontological model of reference for the representation of information on cultural heritage in our Question Answering prototype. It provides a common semantic framework for the mapping of cultural heritage information and can be adopted by museums, libraries and archives.

We designed and implemented a system capable of interpreting natural
language questions regarding cultural heritage objects and facts, map the input
questions into formal queries compliant to the CIDOC-crm model and execute
such queries to retrieve the desired information.

We believe that the choice of using CIDOC-crm is valid because of the following key factors:
\begin{itemize}
\item Museums and institutions typically have structured sources in which they store information about their artifacts
\item The availability of documentary sources is limited. If we take into consideration freely available documentary sources such as Wikipedia, we realize that the percentage coverage of works and authors can only be very low. For example, a museum like the British Museum has about 8 million artifacts while on Wikipedia there are around 500 thousand articles about works of art (from all over the world)
\item The CIDOC-crm model has been specifically designed as a common language for the exchange and sharing of data on cultural heritage without loss of meaning, supporting the implementation of local data transformation algorithms towards this model
\item The CIDOC-crm is a standardized maintained model, and is periodically released in RDFs format
\end{itemize}

In summary, the Question Answering system has therefore the task of finding the information required by the user questions on an RDF knowledge-base that follows the CIDOC-crm model. The query language of RDF is SPARQL. So, in first approximation we can say that the Question Answering system transforms natural language questions into SPARQL queries. Our apporach follows a waterfall model in which the user question is first processed from a syntactic point of view and then from a semantic point of view. Our syntactic processing model focuses on a concept of \emph{template}, where a template represents a category of syntactically homogeneous questions.
In our system, templates are encoded in terms of Answer Set Programming rules. By using ASP we can work in a declarative fashion and avoid implementing the template matching procedure from scratch.
 The semantic processing is instead focused on a concept of \emph{intent}, where by intent we mean the purpose (i.e. the intent) of the question: two questions can belong to two disjoint syntactical categories, but have the same intent and vice versa. To give an example: \emph{who created Guernica?} and \emph{who is the author of Guernica?} have a quite different syntactic structure, but have the same intent, i.e. \emph{know who made the work Guernica}. On the other hand, if we consider \emph{who created Guernica?} and \emph{who restored Guernica?} we can talk about questions that are syntactically similar (or homogeneous), but semantically different: the purpose of the two questions is different.
 Finally, in the very last chapters of the thesis we present a preliminary architecture and implementation for 

\chapter{Preliminaries on Question Answering}

Question Answering is a long-standing computer science discipline that is usually referred to in the field of Information Retrieval and Natural Language Processing. The first prototypes of Question Answering systems date back to the 60s: Lunar and Baseball. These two systems implemented the two main paradigms question answering: answer to questions based on IR and answer to questions on structured knowledge bases. Countless Question Answering systems have been developed over the years. One of the most famous is IBM Watson that in 2011 beat human competitors at the American television game Jeopardy and although the goal of the game was entertainment, the technology used to answer the questions of the television quiz led to an effective advancement of the techniques of QA.
Most QA systems focus on factual questions, which are questions whose answer can be expressed in terms of a short text that represents a fact. The following questions fall into this category and can be answered in simple facts that denote respectively a person, an artwork and a place:
\begin{itemize}
\item Who painted Guernica?
\item Which Van Gogh artwork represents sunflowers?
\item Which museum houses the Rosetta Stone?
\end{itemize}
Below we will briefly talk about the two main paradigms of QA focusing on their application in answering factual questions and we will focus on the type that is better suited to out context.
The first paradigm is called Information Retrieval Question Answering or also with the simple name of text-based Question Answering. This type of systems typically rely on a vast amount of information found in the form of text on the Web or in specific document collections. Given a user question, the QA system uses techniques similar to those of IR to extract passages directly from the documents, guided by the text of the input question. The second type of paradigm is known as Knowledge-Based Question Answering and is based on a semantic representation of the question in the form of a formal query on a knowledge base.

\section{IR-based Question Answering}

The models of QA that are based on Information Retrieval respond to user questions by searching for answers in text segments taken from the Web or other document collections. Typically, the flow of execution of an IR-based QA system follows the following steps:
\begin{enumerate}
\item Question processing
\item Question classification
\item Query formulation
\item Passage extraction
\item Answer extraction
\end{enumerate}

In the question processing phase, the goal is to extract useful information from the input question. The type of responses specifies the type of entities involved in the response (e.g., people, places, etc.). The query specifies keywords that can be used by techniques similar to those of IR for the retrieval of relevant documents. Some systems perform an initial classification of the type of question at this stage (for example, if it is a question that asks for a definition, a mathematical question or a question from which there is a list of terms as an answer).
In the question classification, the type of expected response is determined. For example, the question \emph{who has painted Guernica?} has a PERSON-like answer, while the question \emph{where was Picasso born?} is expected to have a PLACE-like answer. In this way, once the entity type of the expected answer is known, we can concentrate only on the segments of text that concern such entity type. In this phase, classification systems can be used, also in terms of entity categorization, which is often organized hierarchically following a taxonomy that can be constructed semi-automatically or manually. In this phase, the use of lexical resources such as WordNet is often introduced to facilitate the identification of the entities and identify the related words.
In the query formulation phase, the task is to create a list of keywords to be used in a search process using IR techniques. This phase is influenced by the type of sources available. In the case of Web search, web search engines can sometimes be used directly.
The passage extraction phase starts from the documents retrieved by the execution of the query built in the previous phase and further narrows down the search space by analyzing the most promising documents and extracting the text passages that potentially contain the answer. Typically this phase follows a supervised approach where the features are relevant features of the individual steps through a multi-level match with the input question.
Finally, the answer extraction phase typically involves an extractive process which consists in extracting a specific portion of the text that has been selected in the previous steps. Two common answer extraction techniques are answer-type pattern extraction and N-gram tiling.

\section{Knowledge-Based Question Answering}
Although a vast amount of data is coded in terms of text on the web, information also exists in more structured forms. The term Knowledge-Based Question Answering refers to the problem of answering questions in natural language by querying a structured knowledge base. As is the case with the text-based QA paradigm, the origins of this approach date back to the 1960s with the BASEBALL system, which provided answers by using a structured database of baseball games and statistics. Knowledge-base QA systems typically map questions in the form of predicates computation or query languages such as SQL or SPARQL. Often the knowledge base consists of a relational database or a less structured database such as triple-store databases for RDF. Among the most frequently used methods for question classification we find the approaches based on rules that typically lead to good accuracy, but require a consistent contribution of grammatical rules provided by experts. Another family of widespread approaches is based on supervised methods, where the question classification process is more automated, but large training sets are often required to function properly.

\chapter{A Question Answering System for Cultural Heritage}
In this chapter we will illustrate and motivate the design choices of a Question Answering system, which is consistent with the project objectives and can be implemented in a software system in the project times and constraints.
The idea is to realize a Knowledge-Based Question Answering System following a rules-based approach to obtain high precision in the management of the questions, with the support of valid tools to help the construction of the rules in order to speed up the management process. new questions within the system.
The input of the system is given by the questions asked by the user. In our hypothesis the questions are gradually transformed into formal queries that are then executed and the result of which is transformed into a natural language answer. In our system, the question answering process is split into several phases:
\begin{enumerate}
\item the input question is transformed into a three-level syntactic representation: \textbf{Question NLP Processing}
\item the representation is categorized by a template system that can be implemented by means of logical rules with Answer Set Programming: \textbf{Template Matching}
\item the next phase, managed through imperative code (Java), allows passing from a template to an intent, where the intent identifies precisely the intent (or purpose) of the application: \textbf{Intent Classification}
\item the intent generates a formal query which is performed to find the answer to the question based on knowledge: \textbf{Query Generation}
\item the Query is physically executed on the knowledge-base: \textbf{Query Execution}
\item the result produced by the interrogation is transformed into a natural language answer: \textbf{Answer Generation}
The division of the QA process into distinct phases allows the prototype to be realized in distinct components with low interdependence. The advantage is to simplify the development and allow a better maintainability of the system. In the following sections we will analyze in detail the 6 phases just listed.
\end{enumerate}

\section{Question NL Processing}
The NL processing phase deals with building a formal and therefore tractable representation of the input question. The question is decomposed and analyzed highlighting the atomic components that compose it, the morphological properties of the components and the relationships that bind them. Fortunately, at this stage it is possible to use pre-existing computational tools and models in the field of natural language processing, also for the Italian language.
This phase is in turn divided into sub-processes:
\begin{itemize}
\item Recognition of named entities
\item Tokenization
\item Part-of-speech tagging
\item Dependency parsing
\end{itemize}

\subsection{Named Entities Recognition}
The named entities of a text are portions of text that identify the names of people, organizations, places or other elements that are referenced by a proper name. For example, in the phrase \emph{Michelangelo has painted the Last Judgment} we can recognize two entities, that are Michelangelo that could belong to a \emph{Person} category and the Last Judgment that could belong to a category \emph{Artwork}. The recognition of named entities is a NLP task that deals with identifying and eventually categorizing the entities that appear in a text. In a QA system, a NER phase makes it possible to identify the topics of the input question, classify them and eventually simplify the subsequent phases of NLP. To date, there are several existing implemenations of NER. Mostly, the NER algorithms are based on grammars of natural languages or on statistical approaches such as Machine Learning. Grammar-based approaches typically require a consistent supply of grammar rules provided by grammar experts, offering high accuracy, but low recall and consistent rule-making work that can take months. Instead, statistical approaches require large amounts of training data that are manually annotated. More recently, semi-supervised approaches have been developed to allow faster creation of the training set.
When the entities of the text have been recognized, they can be replaced with placeholders that are easier to manage in the subsequent stages of processing of natural language. For example, it is possible to replace long and decomposed names with atomic names (that is, composed of a single word) that are more easily handled during tokenization, Parts-of-speech tagging and Dependency Parsing.
\begin{figure}
 \centering
    \includegraphics[width=0.9\textwidth]{figures/ner}
\caption{Example of Named-Entities Recognition}\label{fig:ner}
\end{figure}

\subsection{Tokenization}
Tokenization consists of splitting text into words (called tokens). A token is an indivisible unit of text. Tokens are separated by spaces or punctuation marks. In Italian, as in other western languages, the tokenization phase turns out to be rather simple, as these languages place quite clear word demarcations. In fact, the approaches used for natural language tokenization are based on simple regular expressions. Tokenization is the first phase of lexical analysis and creates the input for the next Part-of-Speech Tagging phase.
Figure \ref{fig:tokenization} shows an example of tokenization.
\begin{figure}%[ht!]
 \centering
    \includegraphics[width=0.65\textwidth]{figures/tokenization}
\caption{Example of tokenization}\label{fig:tokenization}
\end{figure}

\subsection{Parts-Of-Speech Tagging}
The part-of-speech tagging phase consists in assigning to each word the corresponding part of the speech. Common examples of parts-of-speech are adjectives, nouns, pronouns, verbs, or articles. The part-of-speech assignment is typically implemented with supervised statistical methods. There are, for several languages, large manually annotated corpora that can be used as training sets to train a statistical system. Among the best performing approaches are those based on Maximum Entropy [6]. The set of possible parts of the speech (called tag-set) is not fixed, and above all it can present substantial differences depending on the language taken into consideration. For Italian, a reference tag-set is the Tanl tag-set: \url{http://medialab.di.unipi.it/wiki/Tanl_POS_Tagset}. This tag-set distinguishes between coarse-grained tags and fine-grained tags and allows to specify tags that include morphological information such as gender and number.
Figure \ref{fig:pos} shows an example of POS tagging. In the example the symbols above the words indicate the tags corresponding to the words below: \emph{NP} indicates a proper name, \emph{VB} indicates a verb, and \emph{F} indicates a punctuation mark.
\begin{figure}
 \centering
    \includegraphics[width=0.65\textwidth]{figures/pos}
\caption{Example of POS-tagging}\label{fig:pos}
\end{figure}

\subsection{Dependency Parsing}
Dependency Parsing is the identification of lexical dependencies of the words of a text according to a grammar of dependencies. The dependency grammar (DG) is a class of syntactic theories that are all based on the dependency relationship (as opposed to the circumscription relation). Dependency is the notion that linguistic units, e.g. words, are connected to one another by directed connections (or dependencies). A dependency is determined by the relationship between a word (a head) and its dependencies. The methods for extracting grammar dependencies are typically supervised and use a reference tag-set and a standard input representation format known as the CoNLL standard, developed and updated within the CoNLL scientific conference (Conference on Computational Natural Language Learning).
An updated version of the CoNLL format is the CoNLL-U version. In CoNLL-U, lexical information is encoded as plain text UTF-8. The information is organized by line and there are 3 types of line:
\begin{itemize}
\item Word lines: contain annotations related to a word (token) in 10 fields separated by single tabs.
\item Blank lines: used to separate sentences.
\item Comment lines: start with a hash symbol (\#) and are not interpreted by the interpreters of the CoNLL format, but are used to add comments.
\end{itemize}
A sentence consists of one or more word lines. A word line contains the following attributes:
\begin{itemize}
\item ID: Index of the word, is an integer starting from 1 for each new sentence; it can cover a multi-word token
\item FORM: Shape of the word (that is the word itself), possibly a punctuation symbol
\item LEMMA: Lemma or stem or canonical form of the word. E.g. run is the lemma of running, apple is the lemma of apples
\item UPOSTAG: universal part-of-speech tag
\item XPOSTAG: language-specific part-of-speech tag
\item FEATS: (Features) List of morphological characteristics associated with the word (eg gender, number, time)
\item HEAD: Head of the current word. It allows to represent a direct grammatical dependency; it is the ID of the related word (head)
\item DEPREL: tag that identifies the type of grammatical dependency
\item DEPS: used for the analysis of the dependencies whose structure is not a tree, but rather a graph and there may be more arcs coming out of a node
\item MISC: any other annotation
\end{itemize}
\begin{figure}
 \centering
    \includegraphics[width=1\textwidth]{figures/td}
\caption{Example of Dependency Parsing}\label{fig:td}
\end{figure}

\section{Template Matching}

We apply tokenization, parts-of-speech tagging and dependency parsing to input questions and get a three-level representation that is then encoded in the form of ASP facts. This three-level representation is the input of the template matching phase.

Templates identify categories of questions that are uniform from the syntactic point of view and we express them in the form of ASP rules. Given a question $Q$ and a template $T$, we say that $Q$ satisfies $T$ if $Q$ is a ground instantiation of $T$. We refer to chapter \ref{chapter:asp} for a reference of ASP.

Consider the question \emph{who painted Guernica?}: the NLP phase would produce these atoms (also called facts):

\begin{program}
\item[] {\tt word(1, "who"), word(2, "painted").}
\item[] {\tt word(3, "Guernica"). word(4, "?").}
\item[] {\tt pos(1, PR), pos(2, VB). pos(3, NP). pos(4, F).}
\item[] {\tt gr(2, 1, nsubj), gr(2, 3, dobj). gr(2, 4, punct).}
\end{program}

Words are encoded by the \emph{word} predicate, parts of the speech by the \emph{pos}
 predicate, and finally the grammatical relations (a.k.a. typed dependencies) are encoded by the \emph{gr} predicate. Numerical terms represent the positions of words in the question text. For example, \emph{pos(3, NP)} indicates that the third word is a proper noun (NP tag), meaning that Guernica is a proper noun.
The body literals of a template can be both ground and non ground and the predicates used are the same with which the question is represented (word, pos, gr). Moreover we use two auxiliary predicates \emph{lemma} which allows to encode word-lemma relations, where the lemma of a word is its canonical form. The use of non-ground atoms in bodies allows, first of all, to extract terms of the input question (in this case, variable also appear in the rule head) and secondly  to write more flexible rules.
Finally we introduced a concept of template weight, as a numerical value that express the importance of a template, by using template weights we can express template preferences and this is especially useful to favor more specific templates with respect to more generic templates.

Consider the following template that allows to capture questions of the form \emph{who action object?}:
\begin{program}
\item[] {\tt template(who\_action\_object, bt(V, W), 8) :- lemma (1, “chi”),} 
\item[] {\tt word(2, V), word(3, W), word(4, “?”),}
\item[] {\tt pos(1, PR), pos(2, VB), pos(3, NP), pos(4, F),}
\item[] {\tt gr(2, 1, nsubj), gr(2, 3, dobj).}
\end{program}

In the example, \emph{who\_action\_object} is a constant that identifies the template, while $bt(V, W)$ is a function symbol that allows to extract the terms of the input question, respectively the verb $V$ and the object $W$.
This weight of the example template is 8. The weight of a template reasonably expresses how many clauses are in the template; the higher the weight the better the template describes the question. The system automatically determines (by means of ASP rules) the best template (or the best ones, with the same weight). The best template is then the output of the template matching phase.
Figure \ref{fig:template-example} exemplifies the template matching process applied to the example described above. The program answer set determines the best templates that match the input question.

\begin{figure}
 \centering
    \includegraphics[width=1\textwidth]{figures/template_matching}
\caption{Template matching example}\label{fig:td}
\end{figure}

\section{Intent Classification}
The classification of the question by templates is typically not sufficient to identify its purpose. The intent classification phase (or determination of intent) deals with identifying the intent (or purpose) of the input question, that is, what the question asks, starting from the result of the template matching phase.

The intent classification process is based on lexical expansion of question terms and allows to disambiguate the intent of questions that fall into the same syntactic category (and that therefore have a match on the same template) and also allows us to make the templates more generic. For example, \emph{who painted Guernica?} and \emph{who killed Caesar?} have a very similar syntactic structure and may fall into the same template. Extracting the verb and making semantic expansions allows to disambiguate the intent. Painting is in fact a hyponym of creating and this fact allows us to understand that the intent is to determine the creator of a work, while killing does not have such relation and we should therefore instantiate a different intent.
In the same way \emph{who painted Guernica?}, \emph{who made Guernica?} or \emph{who created Guernica?} are all questions that can be correctly mapped with a single template and are correctly recognized by the same intent thanks to the fact that all three verbs are hyponyms or synonyms of the verb \emph{create}.
Among the semantic relationships that could be taken into consideration are:
\begin{itemize}
\item \textbf{Synonymy} that indicates the relationship between two words with the same meaning
\item \textbf{Hyperonymy} which indicates a semantic field more extensive than a word than another. For example, mobile is a hyper-synonym of a chair, creating is a hyper-color of painting
\item \textbf{Hyponymy} which is the inverse relation of hyperonymy
\item \textbf{Meronymy} indicating a part-whole relationship. For example, finger is a moronym of hand, change is a meronym of restoration
\item \textbf{Olonimy} which is the inverse relationship of meronymy
\end{itemize}
Words semantic relations can be obtained by using dedicated dictionaries, like wordnet[] or BabelNet[].

In our work, we formalize intents as functions, in particular as polydrome functions. A polydrome function is a function that can have multiple values. Each intent is associated with a function name, a domain and a codomain (or range). Being polydrome functions, applying the function to a value in the domain can return any subset of the codomain. Domain and codomain of a function can be considered as instances of the same family of objects: for example, \emph{Person}, \emph{Artwork}, \emph{Time}, \emph{Place}, etcetera.
Formulating intents in terms of functions allows for intent composition, which enables intents reuse. By using composition we can start from atomic intents that correspond to a simple query on the knowledge-base and possibility construct more complex intents as a composition of simpler intents. For example, the intent \emph{birth date of a person} could be decomposed into an intent that identifies the birth event and an intent that identifies the date of an event. It is then possible to re-use the intent \emph{birth of a person} in the intent that determines the place of birth through the composition with another atomic intent \emph{place of an event}. 



\section{Query Generation and Execution}
The transformation of the question into a formal query ends with the Query Generation phase. The Query Generation is based on an intent mapping mechanism in formal queries. In particular,
\begin{itemize}
\item every atomic intent is associated with a SPARQL query
\item the query of a composite intent $I$ is obtained by composing the queries of the intents that make up $I$
\end{itemize}

//TODO describe composition more formally, explain variable clashes

Figure \ref{fig:composition} shows an example of intent composition in SPARLQ, using some properties of the CIDOC-crm model.

\begin{figure}
 \centering
    \includegraphics[width=1\textwidth]{figures/composition}
\caption{Example of intents composition}\label{fig:composition}
\end{figure}

In the Query Execution phase, the previously generated query is executed on the Knowledge Base. The problem of running a query on an RDF knowledge base is a problem for which there are already several solutions. Among the many we mention Apache Jena for Java and Rasqal for C / C++.

\section{Answer Generation}

The final phase of a typical user interaction with the QA system is the so-called Answer Generation. In this phase the results produced by the execution of the query on the knowledge base are transformed into a natural language answer that is finally returned to the user.
To implement this phase we have designed an answer template system that is in some ways similar to the one seen for question templates. In this case the idea is to have a natural language pattern with empty slots that are filled according to the question intent and the terms extracted from the database.
These templates can be expressed in a compact way through a metalanguage that allows expressing sentences with variations according to the subjects of question or answer.
The example below presents a possible answer template for questions concerning the materials of a work.
\begin{itemize}
\item [] \tt{The material\{s:s\}[R] of <Q> \{s:is,p:are\}[R] <R>.}
\end{itemize}
The curly brackets denote a sequence of variants and the square brackets denote the term (or terms) with respect to which the block preceding it refers: $R$ stands for answer (or response) and $Q$ stands for question. A variant consists of a property and a value separated by a colon symbol.
The block delimited by the braces is replaced by the value of the variant appropriate to the term enclosed between square brackets that follows the block. The determination of the appropriate variant can be implemented within the system using, for example, a dictionary of terms. In the example the $s$ variant is for singular forms and the $p$ variant is for plural forms. The variants may possibly be extended into more complex types (possibly organized in hierarchies) and take into consideration other characteristics of the terms of answer and question extracted from appropriate dictionaries of terms or explicitly represented in the knowledge base.
Finally, the $<Q>$ tag will be replaced by the query terms and the $<R>$ tag from the answer terms.
So, supposing we want to apply the response template from the previous example to the fact that the Rosetta Stone is made of granodiorite we would get the answer:
\emph{The material of the Rosetta Stone is granodiorite}.
Or, if we wanted to apply the answer template to the fact that the materials of the Rusper Chalice are gold, enamel and copper the answer would be:
The materials of the Rusper Chalice are gold, enamel, and copper.

\section{Question History and Subject Disambiguation}
The introduction of contextual elements in a Question Answering system allows for a more interactive experience and allows the disambiguation of some possible questions.
Among the contextual information that can be introduced in an advanced system for querying cultural heritage we mention the history of user questions or data about the user's physical context. In an ideal implementation, this information makes it possible to expand the questions posed by the user, enriching them with contextual content and thus improving the effectiveness of the Question Answering system.
Figure \ref{fig:history} shows a sequence diagram showing a possible semantic expansion of the input question with contextual information, by consulting the questions history. In the example, the question \emph{where was it found?} has a missing, subject and therefore, without considering the context, the question would be ambiguous. Using the user's question history it is possible to disambiguate the subject and understand that the question refers to the Rosetta Stone. It should be noted that disambiguation could be performed both on the subject of the question (as in the example), but also on the subject of the answer.

\section{Overview and discussion}
Figure \ref{fig:qa_architecture} summarizes the architecture of the Question Answering System with an example of user interaction. 
One of the main advantages of the presented architecture is that it provides fine-grained control over the question understanding process. Indeed, the template matching mechanism allows for the addition of new question forms by simply adding a corresponding template. The system favors components reuse, and integrates existing tools and models for NLP that, thanks to the NLP standards such as the CoNLL standard, can be replaced at a later time with better performing modules. On contrast, the limits of the presented approach are related to the fact that 

\begin{figure}
 \centering
    \includegraphics[width=1\textwidth]{figures/qa_architecture}
\caption{Scenario of interaction with the Question Answering System}\label{fig:qa_architecture}
\end{figure}

\section{Related Works}


\chapter{Conclusions}

In this last part of the thesis we tackled the problem of Question Answering in Closed Domains, with a specific focus on the Cultural Heritage domain. We proposed a solution that is based on logic programming and is materialized as a software prototype. 
