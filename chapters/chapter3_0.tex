\hyphenation{either}
\providecommand\AMSLaTeX{AMS\,\LaTeX}
\newcommand\eg{\emph{e.g.}\ }
\newcommand\etc{\emph{etc.}}
\newcommand\bcmdtab{\noindent\bgroup\tabcolsep=0pt%
  \begin{tabular}{@{}p{10pc}@{}p{20pc}@{}}}
\newcommand\ecmdtab{\end{tabular}\egroup}
\newcommand\rch[1]{$\longrightarrow\rlap{$#1$}$\hspace{1em}}
\newcommand\lra{\ensuremath{\quad\longrightarrow\quad}}

\newcommand\lar{\ensuremath{\,\leftarrow\,}}
\newcommand\mi[1]{\ensuremath{\mathit{#1}}}
\newcommand\wasp{\textsc{wasp}\xspace}
\newcommand\clasp{\textsc{clasp}\xspace}
\newcommand\clingo{\textsc{clingo}\xspace}
\newcommand\gringo{\textsc{gringo}\xspace}
%\newcommand\todo[1]{{\Large {\bf TODO:} \textcolor{red}{#1}}}
\ifdraft
\else
\newcommand\todo[1]{}
\fi
\newcommand\lazy{\textsc{lazy}\xspace}
\newcommand\eager{\textsc{eager}\xspace}
\newcommand\post{\textsc{post}\xspace}

\newcommand\posOf[1]{\ensuremath{#1_{|_+}}}

\newcommand{\GP}[1]{\ensuremath{Ground(#1)}\xspace}
\newcommand{\HB}[1]{\ensuremath{B_{#1}}\xspace}
\newcommand{\HU}[1]{\ensuremath{U_{#1}}\xspace}

\newtheorem{example}{Example}
\def\naf{\ensuremath{\raise.17ex\hbox{\ensuremath{\scriptstyle\mathtt{\sim}}}}\xspace}

\newtheorem{lemma}{Lemma}[section]
%\chapter{Introduction to part II}
Answer set programming (ASP) is a declarative formalism for knowledge representation and reasoning based on stable model semantics \cite{DBLP:journals/ngc/GelfondL91,DBLP:journals/cacm/BrewkaET11}, for which robust and efficient implementations are available~\cite{DBLP:conf/iclp/GebserKKOSW16}.
%
State-of-the-art ASP systems are usually based on the ``ground+solve'' approach~\cite{DBLP:journals/aim/KaufmannLPS16}, in which a \textit{grounder} module transforms the input program (containing variables) in an equivalent variable-free one, whose stable models are subsequently computed by the \textit{solver} module.
%
ASP implementations adopting this traditional approach are known to be effective for solving complex problems arising from academic and industrial applications, including: product configuration~\cite{DBLP:conf/scm/KojoMS03}, decision support systems for space shuttle flight controllers~\cite{DBLP:conf/asp/NogueiraBGWB01}, explanation of biomedical queries~\cite{DBLP:journals/tplp/ErdemO15}, construction of phylogenetic supertrees~\cite{DBLP:journals/tplp/KoponenOJS15}, data-integration~\cite{DBLP:journals/tplp/MannaRT15}, reconfiguration systems~\cite{DBLP:conf/cpaior/AschingerDFGJRT11}, and more.
%
Nonetheless, there are some classes of programs whose evaluation is not feasible with the ``ground+solve'' approach.
%(cf.\ \cite{DBLP:journals/tplp/CalimeriIR14,DBLP:journals/ai/CalimeriGMR16}) 
One notable case is due to a combinatorial blow-up of the grounding phase (cf.\ \cite{DBLP:journals/ai/CalimeriGMR16}). An issue that is usually referred to as the \textit{grounding bottleneck} of ASP.
%
In this first part of this thesis, we present some alternative approaches for solving ASP programs, that attempt to succeed  where the standard ``ground+solve'' approach fails or can be improved, especially in presence of the \textit{grounding bottleneck}.

This part is organized as follows: 
\begin{itemize}
\item in chapter \ref{chap:asp} we provide some preliminaries on ASP;
\item in chapter \ref{chap:lazy} we empirically compare the ``ground+solve'' approach with lazy instantiation and custom propagators for solving ASP programs with problematic constraints; 
\item in chapter \ref{chap:compilation} we present partial compilation of ASP programs, that is the application of compilation-based technique to the evaluation of ASP programs (or parts of them).
\end{itemize}



\chapter{Preliminaries and Notation}\label{chap:asp}
An ASP program $\Pi$ is a finite set of rules of the form:
%
\begin{equation}\label{eq:rule}
  a_1 \lor \ldots \lor a_n \lar b_1, \ldots, b_j, \naf b_{j+1}, \ldots, \naf b_m
\end{equation}
%
where $a_1,\ldots,a_n,b_1,\ldots,b_m$ are atoms and $n\geq 0,$
$m\geq j\geq 0$.
In particular, an \emph{atom} is an expression of the form $p(t_1, \ldots, t_k)$, where $p$ is a predicate symbol and $t_1, \ldots, t_k$ are \emph{terms}. 
%
Terms are alphanumeric strings, and are distinguished in variables and constants.
According to the Prolog's convention, only variables start with an uppercase letter.
%1
A \emph{literal} is an atom $a_i$ (positive) or its negation $\naf a_i$ (negative), where $\naf$ denotes the \emph{negation as failure}.
Given a rule $r$ of the form (\ref{eq:rule}), the disjunction $a_1 \lor \ldots \lor a_n$ is the {\em head} of $r$, while $b_1,\ldots,b_j, \naf b_{j+1}, \ldots, \naf b_m$ is the {\em body} of $r$, of which $b_1,\ldots,b_j$ is the {\em positive body}, and $\naf b_{j+1}, \ldots, \naf b_m$ is the {\em negative body} of $r$.
A rule $r$ of the form (\ref{eq:rule}) is called a \textit{fact} if $m=0$ and a \textit{constraint} if $n=0$.
%
An object (atom, rule, etc.) is called {\em ground} or {\em
propositional}, if it contains no variables. 
Rules and programs are \textit{positive} if they contain no negative literals, and \textit{general} otherwise. %if they might contain negative literals.
Given a program $\Pi$, let the \emph{Herbrand Universe} \HU{\Pi} be the
set of all constants appearing in $\Pi$ and the \emph{Herbrand Base}
\HB{\Pi} be the set of all possible ground atoms which can be constructed
from the predicate symbols appearing in $\Pi$ with the constants of \HU{\Pi}.
%Given a ground rule $r$ the \textit{nogood representation} of $r$ is 
%$C(r) = \{ \naf a_1, \ldots, \naf a_n, b_1, \ldots, b_j, \naf b_{j+1}, \ldots, \naf b_m \}$.
Given a rule $r$, \GP{r} denotes the
set of rules obtained by applying all possible substitutions $\sigma$
from the variables in $r$ to elements of \HU{\Pi}. Similarly, given a
program $\Pi$, the {\em ground instantiation} \GP{\Pi} of $\Pi$
is the set \( \bigcup_{r \in \Pi} \GP{r} \).

For every program $\Pi$, its stable models are defined using its ground
instantiation \GP{\Pi} in two steps: First stable models of positive
programs are defined, then a reduction of general programs to positive
ones is given, which is used to define stable models of general
programs.

A set $L$ of ground literals is said to be {\em consistent} if, for every
literal $\ell \in L$, its negated literal $\naf \ell$ is not contained in
$L$. 
Given a set of ground literals $L$, $\posOf{L} \subseteq L$ denotes 
the set of positive literals in $L$.
%we write $S_1 \preceq S_2$ if for all positive literal $a\in S_1$ it holds that $a \in I_2$.
An interpretation $I$ for $\Pi$ is a consistent set of ground literals
over atoms in $\HB{\Pi}$. %\footnote{We represent interpretations as set of literals,
%since we have to deal with partial interpretations in the next sections.}
A ground literal $\ell$ is {\em true} w.r.t.\ $I$ if $\ell\in I$;
$\ell$ is {\em false} w.r.t.\ $I$ if its negated literal is in $I$;
$\ell$ is {\em undefined} w.r.t.\ $I$ if it is neither true nor false w.r.t.\ $I$.
A constraint $c$ is said to be \textit{violated} by an interpretation $I$ if all literals in the body of $c$ are true.
An interpretation $I$ is {\em total} if, for each atom $a$ in $\HB{\Pi}$,
either $a$ or $\naf a$ is in $I$ (i.e., no atom in $\HB{\Pi}$ is undefined w.r.t.\ $I$).
Otherwise, it is \textit{partial}.
A total interpretation $M$ is a {\em model} for $\Pi$
if, for every $r \in \GP{\Pi}$, at
least one literal in the head of $r$ is true w.r.t.\ $M$ whenever all literals in the
body of $r$ are true w.r.t.\ $M$.
A model $X$ is a {\em stable model}
for a positive program $\Pi$ if any other model $Y$ of $\Pi$ is such that $\posOf{X} \subseteq \posOf{Y}$.
%w.r.t.\ set inclusion among the models of $\Pi$. 
%\todo{minimal set inclusion but interpretations of literals}

The {\em reduct} or {\em Gelfond-Lifschitz transform} 
%\todo{We can just use reduct, no there are several reducts}
of a general ground program $\Pi$ w.r.t.\ an interpretation $X$ is the positive
ground program $\Pi^X$, obtained from $\Pi$ by (i) deleting all rules
$r \in \Pi$ whose negative body is false w.r.t.\ X and (ii)
deleting the negative body from the remaining rules.
A stable model of %a general program
$\Pi$ is a model $X$ of $\Pi$ such
that $X$ is a stable model of $\GP{\Pi}^X$. 
%The set of all stable models of $\Pi$ is denoted by $SM(\Pi)$.
%A program $\Pi$ is \textit{coherent} if $SM(\Pi) \neq \emptyset$, and \textit{incoherent} otherwise.
We denote by $SM(\Pi)$ the set of all stable models of $\Pi$,
and call $\Pi$ \textit{coherent} if $SM(\Pi) \neq \emptyset$, \textit{incoherent} otherwise.

%\todo{We need: 1) grounding, example1 can be used as example of the grounding 2) partial interpretations 3) the nogood representation of a rule 4) supportedness property 5) Define SM($\Pi$) the set of all stable models}
%A program is a finite set of rules of the form
%\begin{align*}
%  a_1 \lor \cdots \lor a_n \lar b_1,\ldots,b_j, \naf b_{j+1}, \ldots, \naf b_m.
%\end{align*}

\begin{example}\label{ex:grounding}
	Consider the following program $\Pi_1$:
	\begin{equation*}
	\begin{array}{lll}
	r_1: a(1) \leftarrow \naf b(1) &\qquad 
	r_2: b(1) \leftarrow \naf a(1) & \qquad
	r_3: \leftarrow a(X), \ b(X) \\
	r_4: c(1) \leftarrow \naf d(1) & \qquad
	r_5: d(1) \leftarrow \naf c(1) & \qquad
	r_6: \leftarrow a(X), \ \naf b(X)
	\end{array}
	\end{equation*}	
	The ground instantiation $\GP{\Pi_1}$ of the program $\Pi_1$ is the following program:
		\begin{equation*}
	\begin{array}{lll}
	g_1: a(1) \leftarrow \naf b(1) &\qquad 
	g_2: b(1) \leftarrow \naf a(1) & \qquad
	g_3: \leftarrow a(1), \ b(1) \\
	g_4: c(1) \leftarrow \naf d(1) & \qquad
	g_5: d(1) \leftarrow \naf c(1) & \qquad
	g_6: \leftarrow a(1), \ \naf b(1)
	\end{array}
	\end{equation*}
	Note that %the (total) interpretation
  $M=\{\naf a(1), \ b(1), \ c(1), \ \naf d(1)\}$ is a model of $\GP{\Pi_1}$. Since $\GP{\Pi_1}^M$ comprises only the facts $b(1)$ and $c(1)$, and constraint $g_3$, $M$ is a stable model of $\Pi$. 
$\hfill$
\end{example}

%\paragraph{Support.}
%%
%To compute stable models, solvers often use 
%the following two properties
%(for details see \cite{Gebser2012aspbook}).
%Each atom $a$ in a stable model $I$ is \emph{supported}
%such that at least one rule with $a$ in the head
%fires w.r.t.\ $I$.
%A stable model cannot contain an \emph{unfounded set}
%which is a set of atoms that is supported circularly
%(i.e., self-supported) and where a smaller set of true atoms
%is a stable model.
%\todo{PS: wrote a short paragraph about support/unfounded and cited Gebser ASP Book as reference to address reviewer concerns - also did this for clark completion}

\paragraph{Support.}
Given a model $M$ for a ground program $\Pi$, 
we say that a ground atom $a \in M$ is {\em supported}
with respect to $M$ if there exists a \emph{supporting} rule $r\in \Pi$
such that $a$ is the only true atom w.r.t. $M$ in the head of $r$, and all literals in the
body of $r$ are true w.r.t.\ $M$.
If $M$ is a stable model of a program $\Pi$, then all atoms in $M$ are supported.

%\paragraph{Unfounded sets.}
%Let $I$ be a (partial) interpretation for a program $\Pi$. 
%A set $U \subseteq \HB{\Pi}$ of ground atoms is an {\em unfounded set} for $\Pi$ w.r.t. $I$ if, for each $a \in U$ and for each rule $r \in \GP{\Pi}$ such that $a$ occurs in the head of $r$, at least one of the following conditions holds: the body of $r$ contains some false literal 
%w.r.t. $I$; some positive literal in the body of $r$ belongs to $U$; an atom in the head
%of $r$, distinct from $a$ and other elements in $U$, is true w.r.t. $I$.
%Intuitively, atoms in an unfounded set cyclically depend on each other.
%If $M$ is a stable model of a program $\Pi$, then no nonempty $M' \subseteq \posOf{M}$ is an unfounded set.

