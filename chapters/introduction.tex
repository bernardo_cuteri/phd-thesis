%This works originated from the intention of developing a natural language understanding solution to a real-world problem and presents solutions to that, then by connected problems related to the natural language challenge, dealt with real-worlds issues in logic programing


%This work consists of two parts that represent two efforts in the field of artificial intelligence. 
\selectlanguage{italian}
\begin{abstract}
Questo lavoro è incentrato sull'Answer Set Programming (ASP), il quale è un formalismo espressivo per la rappresentazione della conoscenza e il ragionamento automatico. Nel corso del tempo, ASP è stato sempre più dedicato alla risoluzione di problemi reali grazie alla disponibilità di sistemi efficienti.
Questa tesi presenta due contributi in questo ambito: \emph{(i)} nuove strategie per migliorare la valutazione dei programmi ASP, e \emph{(ii)} un'applicazione di ASP per il Question Answering in Linguaggio Naturale.

Nel primo contributo studiamo alcuni casi in cui la strategia di valutazione classica di ASP fallisce a causa del cosiddetto \emph{grounding bottleneck}. Dapprima ci concentriamo su casi in cui ci sono dei vincoli la cui istanziazione è problematica. Affrontiamo il problema usando propagatori ad-hoc e istanziatori lazy, e dimostriamo empiricamente sotto quali condizioni queste soluzioni sono efficaci. Due svantaggi delle tecniche basate sui propagatori/istanziatori sono la necessità di avere una conoscenza approfondita dei sistemi ASP e il fatto che l'implementazione sia a carico dell'utente e scritta in un lionguaggio non dichiarativo. Per superare questi svantaggi, abbiamo ideato una tecnica di compilazione parziale dei programmi ASP. Questa nuova tecnica consente di generare automaticamente i propagatori per alcune delle regole logiche di un programma.
Un'analisi empirica mostra i benefici, in termini di prestazioni, che possono essere ottenuti introducendo la compilazione parziale nella valutazione dei programmi ASP.
Per quanto ne sappiamo, questo è il primo lavoro su tecniche di compilazione per programmi ASP.

Per quanto riguarda la seconda parte della tesi, presentiamo un sistema di risposta alle domande in linguaggio naturale la cui implementazione è basata su ASP.
Il sistema da noi realizzato trasforma le domande di input in query SPARQL che vengono eseguite su una base di conoscenza ontologica. Nel sistema sono integrati diversi modelli e strumenti allo stato dell'arte per il processamento del linguaggio naturale con particolare riferimento alla lingua italiana e al dominio dei beni culturali.

Il sistema che è stato da noi realizzato rappresenta il modulo software principale nell'ambito del progetto PIUCULTURA che è un progetto finanziato dal Ministero dello Sviluppo Economico italiano e il cui obiettivo è quello di promuovere e migliorare la fruizione dei beni culturali.
\end{abstract}

\selectlanguage{english}
\begin{abstract}
This work is focused on Answer Set Programming (ASP), that is an expressive formalism for Knowledge Representation and Reasoning. Over time, ASP has been more and more devoted to solving real-world problems thanks to the availability of efficient systems.
This thesis brings two main contributions in this context: 
\emph{(i)} novel strategies for improving ASP programs evaluation, and \emph{(ii)} a real-world application of ASP to Question Answering in Natural Language. 

Concerning the first contribution, we study some cases in which classical evaluation fails because of the so-called \emph{grounding bottleneck}. In particular, we first focus on cases in which the standard evaluation strategy is ineffective due to the grounding of problematic constraints. We approach the problem using custom propagators and lazy instantiators, proving empirically when this solution is effective, which is an aspect that was never made clear in the existing literature. 
Despite the development of propagators can be effective, it has two main disadvantages:
it requires deep knowledge of the ASP systems, and the resulting solution is not declarative.
We propose a technique for overcoming these issues which we call program compilation. 
In our approach, the propagators for some of the logic rules (not only for the constraints) of a program are generated automatically by a compiler. 
We provide some sufficient conditions for identifying the rules that can be compiled in an approach that fits a propagator-based system architecture.
An empirical analysis shows the performance benefits obtained by introducing (partial) compilation into ASP programs evaluation.
To the best of our knowledge, this is the first work on compilation-based techniques for ASP.

Concerning the second part of the thesis, we present the development of a Natural Language Question Answering System whose core is based on ASP. 
The proposed system gradually transforms input questions into SPARQL queries that are executed on an ontological knowledge base.
The system integrates several state-of-the NLP models and tools with a special focus on the Italian language and the Cultural Heritage domain.
ASP is used to classify questions from a syntactical point of view. 
%
The resulting system is the core module of the PIUCULTURA project, funded by the Italian Ministry of Economic Development, that has the aim to devise a system for promoting and improving the fruition of Cultural Heritage.

%We empirically show that custom propagators can be effective for some classes of programs and then we focus on the possibility to generate custom propagators automatically, focusing on \emph{lazy propagators}. In an attempt to generate efficient propagators we introduced partial compilation of ASP programs.  We developed a partial compilation technique that can be adopted in cases that go beyond lazy propagators of constraints by adding ASP rules under certain conditions. 
\end{abstract}

\chapter*{Introduction}
\paragraph*{Context and motivation.}
Answer Set Programming (ASP) is a powerful formalism that has roots in Knowledge Representation and Reasoning and is based on the stable model semantics~\cite{DBLP:journals/ngc/GelfondL91,DBLP:journals/cacm/BrewkaET11}. 
%
ASP is a viable solution for representing and solving many classes of problems thanks to its high expressive power and the availability of efficient systems~\cite{DBLP:conf/ijcai/GebserLMPRS18}.
Indeed, ASP has been successfully applied to several academic and industrial applications such as product configuration~\cite{DBLP:conf/scm/KojoMS03}, decision support systems for space shuttle flight controllers~\cite{DBLP:conf/asp/NogueiraBGWB01}, explanation of biomedical queries~\cite{DBLP:journals/tplp/ErdemO15}, construction of phylogenetic supertrees~\cite{DBLP:journals/tplp/KoponenOJS15}, data-integration~\cite{DBLP:journals/tplp/MannaRT15}, reconfiguration systems~\cite{DBLP:conf/cpaior/AschingerDFGJRT11}, and more. A key features of ASP consists in the capability to model hard combinatorial problems in a declarative and compact way.

But, how far can the spectrum of applicability of ASP be extended in real-world scenarios? Even though ASP is supported by efficient systems, is there still room for improvements?

On one hand, there is a general need for assessing ASP on a wider and wider spectrum of real-world applications. This is particularly useful to provide a more general understanding of the usability of ASP in practice.
%
On the other hand, the evaluation of ASP programs is a complex issue, and there are still cases where ASP systems can be improved, especially in presence of the so-called \textit{grounding-bottleneck}~\cite{DBLP:journals/ai/CalimeriGMR16}. Thus there is the need to overtake their weak spots and possibly gain performance improvements to keep up with the challenges of emerging applications.

This thesis has two main goals in this context: \emph{(i)} devise novel strategies for improving ASP programs evaluation, and \emph{(ii)} applying ASP to the challenging problem of Question Answering in Natural Language. 
%
These two goals and the related contributions are introduced in the following and treated in detail in the two parts of this thesis. The two parts are related in the sense that they tackle two aspects that are both important to the development of the ASP ecosystem. Indeed, theoretical/methodological results make real-worlds ASP applications possible, and on the other hand, real-world applications push the innovation of existing systems, methods and theories.
%state-of-the-art ASP systems evaluate ASP programs in a two-step computation by first removing variables in the so-called \emph{grounding phase} and then performing a stable models search on the resulting variable-free program. This approach is typically referred to as the ``ground+solve'' approach.
%
%% it can potentially lead to the innovation of ASP systems that have to keep up with emerging trends and use-cases.
%
%One of the main criticism to the ``ground+solve'' approach is that the two phases are completely separated and the grounding of an ASP program might require both in theory and in practice a memory space and an amount of time that exceed usability limits. Not only the grounding phase can be too costly to perform by itself, but also it can generate a propositional program that is too big for solvers to tackle. This problem is often referred to as the \emph{grounding bottleneck} of ASP\cite{DBLP:journals/ai/CalimeriGMR16}. 
%The grounding bottleneck has been the subject of several studies in recent years, and various alternative approaches to overcome it have been proposed such as asperix [42], gasp [13], and omiga [15]. Albeit alternative approaches obtained promising preliminary results, they cannot yet reach the performance of state of the art systems in many benchmarks [12, 42]. 



\paragraph{Part 1: Enhancing ASP Systems on Grounding-intensive Programs}
State-of-the-art ASP systems evaluate ASP programs in a two-step computation by first removing variables in the so-called \emph{grounding phase} and then performing a stable models search on the resulting variable-free program in the so-called \emph{solving phase}. This approach is typically referred to as the ``ground+solve'' approach. One of the main criticism to the ``ground+solve'' approach is that the two phases are completely separated and the grounding of an ASP program might require, both in theory and in practice, a memory space and an amount of time that exceed usability limits. Not only the grounding phase can be too costly to perform by itself, but also it can generate a propositional program that is too big for solvers to tackle. This problem is often referred to as the \emph{grounding bottleneck} of ASP~\cite{DBLP:journals/ai/CalimeriGMR16}. 
The grounding bottleneck has been the subject of several studies in recent years, and various alternative approaches to overcome it have been proposed such as ASPeRiX~\cite{DBLP:conf/lpnmr/LefevreN09a}, GASP~\cite{dal2009gasp}, and OMiGA~\cite{DBLP:conf/jelia/Dao-TranEFWW12}. Albeit alternative approaches obtained promising preliminary results, they cannot yet reach the performance of state of the art systems in many benchmarks~\cite{DBLP:journals/ai/CalimeriGMR16,DBLP:conf/lpnmr/LefevreN09a}.

The first part of this thesis presents some alternative strategies for solving classes of ASP programs where the classical ``ground+solve'' evaluation fails or is inefficient. 
First, we focus on problematic ASP constraints and present three different evaluation strategies that are implemented as programmatic extensions of \textsc{WASP}~\cite{DBLP:conf/lpnmr/AlvianoDFLR13}, that is a state-of-the-art ASP solver. We provide an empirical evaluation conducted on real and synthetic benchmarks that confirms that the usage of custom propagators or lazy instantiators can be effective in presence of problematic ASP constraints. Moreover, we investigate the applicability of a portfolio approach to automatically select the best strategy when more than one solution is available. The result is positive in the sense that, empirically, a naive portfolio approach is faster than the best approach.
Then, we present partial compilation of ASP programs. By using partial compilation we can generate lazy instantiators of constraints automatically. The applicability of partial compilation is not limited to constraints, but allows is extended to rules under some provided conditions.
An empirical analysis shows the performance benefits that can be obtained by introducing partial compilation into ASP programs evaluation.

\paragraph{Part 2: An Application of ASP to Closed-Domain Question Answering.}
Question Answering (QA) attempts to find direct answers to questions posed in natural language. There are two families of QA: in open domain QA there is no restriction to the domain of the questions, while in closed domain QA questions are bound to a specific domain. 

In open domain QA, most systems are based on a combination of Information
Retrieval and NLP techniques~\cite{hirschman2001natural}. Such techniques are applied to a large corpus
of documents: first attempting to retrieve the best documents to look into for the
answer, then selecting the paragraphs which are more likely to bear the desired
answer and finally processing the extracted paragraphs by means of NLP. Many closed domain question answering systems also adopt IR approaches, but in this context, we might benefit from existing structured knowledge.
Some of the very early question answering systems were designed for closed
domains and they were essentially conceived as natural language interfaces to
databases~\cite{green1961baseball}\cite{woods1978semantics}.

Closed Domain QA differs from Open Domain QA mainly because the set of possible questions is more limited and there is a higher chance of being able to rely on a suitable model for representing the data that is specific to the domain at hand. 

In the second part of the thesis, we present a system developed in the context of Closed Domain Question Answering. The idea comes from the PICULTURA project, which is a project funded by the Italian Ministry for Economic Development and for which the University of Calabria is a research partner. The PIUCULTURA project has the intent to promote and improve the fruition of Cultural Heritage and a core module of the project is a Question Answering System that had to be able to answer natural language questions posed by the users on Cultural Heritage. The project was centered on the Italian language and the Cultural Heritage domain, but the approach that we developed is general and can work similarly for other languages and, to some extents, to other domains.

Cultural Heritage can benefit from structured data sources: in this context, data has already started to be saved and shared with common standards. One of the most successful standards is the CIDOC Conceptual Reference Model, that has been identified as the ontological model of reference on cultural heritage for our Question Answering system. It provides a common semantic framework for the mapping of cultural heritage information and can be adopted by museums, libraries, and archives.
We developed a system that gradually transforms input natural language questions into formal queries that are executed on an ontological Knowledge Base. 

The presented system integrates state-of-the-art models and tools for Natural Language Processing (NLP).

ASP plays a crucial role in the syntactic categorization of questions in the form of what we call \emph{question templates} that are expressed in terms of ASP rules.
%
This is indeed a new application scenario for ASP and the proposed solution fits the requirements and the expectations of the companies involved in the PIUCULTURA project that will transform our prototypical system in a full-fledged implementation. 

\paragraph{Contributions.}
Summarizing the main contributions of the thesis are:
\begin{itemize}
\item We identify empirically the cases in which a propagator-based solution can be used effectively to circumvent the grounding-bottleneck in presence of problematic constraints, reporting on an exhaustive experiment that systematically compares the standard ``ground+solve'' approach with several alternatives that are based on propagators. 
\item A technique for partial compilation of ASP programs which is embeddable in state-of-the-art ASP systems.
To the best of our knowledge, this is the first work on compilation-based techniques for ASP programs evaluation.
\item A system for Closed-Domain Question Answering that is based on ASP.
\end{itemize}

\paragraph{Publications.}
Some of the contributions of the thesis have been subject of the following scientific publications.

\begin{itemize}

\item Bernardo Cuteri, Kristian Reale, Francesco Ricca.A Logic-Based Question Answering System for Cultural Heritage. \emph{JELIA 2019}, 526-541, 2019.

\item Bernardo Cuteri, Carmine Dodaro, Francesco Ricca, and Peter Sch{\"{u}}ller. Constraints, lazy constraints, or propagators in ASP solving: An empirical analysis. \emph{TPLP}, 17(5-6):780–799, 2017.

\item Bernardo Cuteri and Francesco Ricca. A compiler for stratified datalog programs: preliminary results. In \emph{SEBD}, volume 2037 of \emph{CEUR Workshop Proceedings}, page 158. CEUR-WS.org, 2017.

\item Bernardo Cuteri, Alessandro Francesco De Rosis, and Francesco Ricca. lp2cpp: A tool for compiling stratified logic programs. In \emph{AI*IA}, volume 10640 of \emph{Lecture Notes in Computer Science}, pages 200–212. Springer, 2017.

\item Bernardo Cuteri. Closed domain question answering for cultural heritage. In \emph{DC@AI*IA}, volume 1769 of \emph{CEUR Workshop Proceedings}, pages 17–22. CEUR-WS.org, 2016.


\end{itemize}